package com.github.lespaul361.ebay939streams;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author David Hamilton
 */
public class ClassStreamBuilder {

    private StringBuilder codeRead = new StringBuilder();
    private StringBuilder codeWrite = new StringBuilder();
    private final List<Method> methods;
    static List<Method> getters = new ArrayList<>();
    static List<Method> setters = new ArrayList<>();
    static List<Method> booleanGetter = new ArrayList<>();

    public ClassStreamBuilder(Class classObject) {
        methods = Arrays.asList(classObject.getMethods());
        for (Method m : methods) {
            if (isBooleanGetter(m)) {
                booleanGetter.add(m);
                continue;
            }
            if (isGetter(m)) {
                getters.add(m);
                continue;
            }
            if (isSetter(m)) {
                setters.add(m);
            }
        }
        for (Method m : setters) {
            Class[] types = m.getParameterTypes();
            for (Class type : types) {
                if (isWrapperType(type)) {
                    boolean b = isWrapperType(m.getReturnType());
                    int i = 0;
                }
            }

        }
    }

    private boolean isGetter(Method method) {
        if (!method.getName().startsWith("get")) {
            return false;
        }
        if (method.getParameterTypes().length != 0) {
            return false;

        }
        if (void.class
                .equals(method.getReturnType())) {

            return false;
        }
        return true;
    }

    private boolean isBooleanGetter(Method method) {
        if (!method.getName().startsWith("is")) {
            return false;
        }
        if (method.getParameterTypes().length != 0) {
            return false;

        }
        if (void.class
                .equals(method.getReturnType())) {

            return false;
        }
        return true;
    }

    private boolean isSetter(Method method) {
        if (!method.getName().startsWith("set")) {
            return false;
        }
        if (method.getParameterTypes().length != 1) {
            return false;
        }
        return true;
    }

    private Method getSetter(Method m, List<Method> geters) {
        String name = getGetterName(m);
        for (Method method : geters) {
            if (method.getName().equalsIgnoreCase(name)) {
                return method;
            }
        }
        return null;
    }

    private String getGetterName(Method m) {
        String name = m.getName();
        name = name.substring(3);
        name = "get" + name;
        return name;
    }

    private String setGetterName(Method m) {
        String name = m.getName();
        name = name.substring(3);
        name = "set" + name;
        return name;
    }

    private String getSetterMethodName(Method m) {
        String name = m.getName();
        return name.substring(3);
    }

    private boolean isPrimitiveType(Method m) {
        Class[] types = m.getParameterTypes();
        for (Class type : types) {
            if (isWrapperType(type)) {
                int i = 0;
            }
        }

        return true;
    }
    private static final Set<Class<?>> WRAPPER_TYPES = getWrapperTypes();

    public static boolean isWrapperType(Class<?> clazz) {
        return WRAPPER_TYPES.contains(clazz);
    }

    private static Set<Class<?>> getWrapperTypes() {
        Set<Class<?>> ret = new HashSet<Class<?>>();
        ret.add(Boolean.class);
        ret.add(Character.class);
        ret.add(Byte.class);
        ret.add(Short.class);
        ret.add(Integer.class);
        ret.add(Long.class);
        ret.add(Float.class);
        ret.add(Double.class);
        ret.add(Void.class);
        return ret;
    }

}
